![Logo](./utils/figures/tenor.gif)

Here is a single application **(Nginx)** used as model to run a **gitlab-ci** pipeline, and that enable an routine for the continuous deployment of the application, triggered by any repository changing in the environment.

# How to start

The idea is to simplify the process more as possibly. But, before start it, please, **configure this required tools at your environment**:

- [**Helm**](https://helm.sh/docs/intro/install/)
- [**Kubectl**]

With this tools installed, let's go ahead with the configuration.

## How to start it?

Be sure that you have the AWS credentials defined at Gitlab ( ** ** ) - at least, you must have this two informations to be able to login:

- **AWS_ACCESS_KEY**= *YOUR AWS ACCESS KEY*
- **AWS_SECRET_KEY**= *YOUR AWS SECRET KEY*

**[ IMPORTANT: ]** To prepare the environment, you need first to run the **./marmita.sh** bash script from ***https://gitlab.com/leoisouza/leoieggli-infra*** repository, to be possible starts all the Terraform plan. 

## The repository content

The repository is divided in specific folders for everything that we need to do:

- **APP**    - Contains the values used by Helm. It contains the chart templates used by Helm ( **deployment.yaml, service.yaml, ...** ).
- **UTILS**  - It hosts some images too (jpeg and gif) used for documentation.

## The application

I choose to use a Helm chart to deploy the application, to be more organized. It is a simple nginx, that will be built on each commit at the master branch by the Gitlab CI tool.

## The deploy process

I was working on a Jenkins instance to be deployed, but at the end I was prefering to use the Gitlab CI Tool - so, I defined a ***.gitlab-ci.yaml*** file that at every commit on the master branch will trigger a new build. The process is simple - it builds a docker container with some specific tools, and using credentials stored at the Gitlab environment variables (masked and secured). After the container creation, it starts a deploy process using Helm.

# References

- **Gitlab CI/CD**
    - https://imasters.com.br/devsecops/gitlab-ci-seu-proprio-runner-privado-com-docker-compose
    - https://opensource.com/article/20/5/helm-charts


I hope you like it!!! 

![Logo](./utils/figures/thankyou.gif)